from setuptools import setup

package_name = 'idl_pf_pkg'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='void',
    maintainer_email='kristoffer.kru@gmail.com',
    description='M.Sc 2021 Indoor Localization Particle filter package',
    license='use and abuse',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'ros_node_pf = idl_pf_pkg.PF_ros_node:main',
        ],
    },
)
