# Other imports
import numpy as np

# Importing pathlib
from pathlib import Path

# Own stuff
from idl_pf_pkg.JitParticleFilterClass import *
from idl_pf_pkg.PFTools import HistogramTools, PointCloudTools
from idl_botsy_pkg.droneConfiguration import DroneGeometry, Rot
from idl_botsy_pkg.filterConfig import MapConfig


class LikelihoodMap():
    def __init__(self):
        ##### Load map & Metadata #####

        # Make map config class, get name of map
        mapConfig = MapConfig()
        mapName = mapConfig.mapName

        # Get path to folder with this file
        home = str(Path.home())

        # Read map metadata
        map_metadata = np.load( home + "/colcon_botsy_idl/src/idl_pf_pkg/idl_pf_pkg/map/metadata_" + mapName + "_10cm_10cm.npy", allow_pickle=True) # Metadata is array of objects, allow_pickle must be true
        
        # Bool to signify if map uses uint8's for probability.
        self.mapUsingUint8Prob = np.bool(map_metadata[4,1])

        # Read map
        if self.mapUsingUint8Prob:
            self.map = np.load( home + "/colcon_botsy_idl/src/idl_pf_pkg/idl_pf_pkg/map/map_" + mapName + "_10cm_10cm.npy").astype(np.uint8)
        else:
            self.map = np.load( home + "/colcon_botsy_idl/src/idl_pf_pkg/idl_pf_pkg/map/map_" + mapName + "_10cm_10cm.npy").astype(np.float32)
        

        ## Parse metadata and save to variables
        # Cartesian offset of cell [0,0,0] from origin
        self.origin_offset = np.array([map_metadata[0,1]]).reshape(3,1)

        # Resolution of cells (side length) [m]
        self.resolution = np.float32(map_metadata[1,1])

        # Size of map in cells [x,y,z]
        self.size_in_cells =  np.int32(map_metadata[2,1])

        # Maximum value possible in map (from gaussian)
        self.mapMaxGaussVal = np.float32(map_metadata[3,1])


class LocalizationFilterParams():
    '''
        Class to hold parameters of Localization Filter
    '''
    
    def __init__(self):
        
        
        # Init drone params and rotator-class
        self.__drone_params = DroneGeometry()
        self.__rot = Rot()

        ############## Const rotations ##############
        self.rotMat_lf = self.__rot.rotX(np.pi)               # Rotmat from level body to filter frame
        self.rotMat_nm = self.__drone_params.rotMat_nm        # Rotmat from ned to map
        ##############################################

        ################### Camera ###################
        self.max_range = 15.0                                 # Max range reading of the depth sensor [m]
        self.pos_b_bc = self.__drone_params.pos_b_bc          # Translation from body frame to cam-frame
        self.rotMat_bc = self.__drone_params.rotMat_bc        # Rotmat from camera to body (camera pitch)

        # Sensor model data [in lack of a better name], NOTE: z_hit + z_rand/z_max = 1
        self.pf_z_hit = 0.8          
        self.pf_z_rand = 0.2
        self.pf_z_max = 1.0
        ###############################################

        ############### Particle Filter ###############
        # PF Params
        self.number_of_particles = 1000
        self.init_pose = np.array([[0.0],[0.0],[0.0],[0.0]],dtype=np.float32)
        self.sigma_pose = np.array([[1.00],[1.00],[1.00],[6.00]],dtype=np.float32)
        self.nPts_PC = 30               # Number of points to sample from pointcloud     
        self.pcUpdateSqSum = False      # Use Square sum method to update weights from pointcloud data
        
        # Use random points when downsampling
        # Selects nPts randomly, and then checks for duplicates and max_range measurements
        # deletes dupes and max_range measurements from pointcloud before passing on
        self.pcdsRandPoints = True

        # Select particles in a loop, checking each point for validity (range, dupe) before adding to an array
        # IF BOTH pcdsLoopSelect AND pcdsRandPoints IS SET TRUE, DEFAULTS TO LOOP CHECK MODE
        self.pcdsLoopSelect = False
        self.pcdsLoopSelMaxLoops = 2*self.nPts_PC

        # Threshold for resampling (resample if effective sample size is less than threshold)
        self.resamplingThreshold = self.number_of_particles



        # Watchdog variables for when no new velocity message arrives, and the filter keeps predicting
        self.wd_counter = 0
        self.wd_counter_decayVelocityTresh = 3
        self.wd_K = 0.01

        ### Maximum values ##
        # Max value for velocity std dev
        self.maxVelStdCtr = np.float32(np.ones((4,1), dtype=np.float32)*2.0)

        # Const variance to add to propogation
        self.constVelVariance = np.array([[0.1],[0.1],[0.1],[0.1]], dtype = np.float32)

        ###############################################
        
        ############# Histogram smoothing #############

        # For histogram smoothing
        self.histogramResolution = np.array([[0.01],[0.01],[0.01],[0.01]], dtype=np.float32)
        kerLen = 5
        self.histSmoothingKernel = np.ones((1,kerLen), dtype = np.float32) / np.float32(kerLen)       # Simple averaging kernel with length 5

        ###############################################

        # initiate Vectors of Velocities, std.deviations and angles from "outside" of filter
        self.velVec_l = np.zeros((4,1), dtype=np.float32)
        self.velStd_l = np.zeros((4,1), dtype=np.float32)
        self.tHeta_bl = np.zeros((3,1), dtype=np.float32)

    def computeGaussianKernel(self, res, sigma):
        '''
            Function so compute a gaussian kernel to the filter, kernel will be 12*(sigma/resolution) + 1 long, (Center value + 6 sigma in each direction)

            Input:
                res     -   Resolution, length between different indices on kernel, float - unit: [m]
                sigma   -   Std deviation of kernel, float - unit: [m]

            Output:
                kernel  -   Normalized gaussian kernel, np.array - size: 1x(12*(sigma/resolution) + 1) 
        '''
        # init kernel
        kernelLength = np.int32(12*sigma/res) + 1
        kernelCenter = np.int32((kernelLength-1)/2)
        kernel = np.zeros((1,kernelLength), np.float32)

        # Compute un-normalized kernel values
        for ii in range( np.int32( (kernelLength+1)/2 ) ):
            # Making use of symmety in kernel around center
            exponent = 1.0/2.0 * (res*ii)**2/(sigma**2)
            kernelValue = np.exp(-exponent)
            kernel[0,kernelCenter + ii] = kernelValue
            kernel[0,kernelCenter - ii] = kernelValue

        # Normalize kernel
        kernelSum = kernel.sum()
        kernel = kernel / kernelSum

        # Set gaussian kernel
        return kernel


class LocalizationFilter():
    
    def __init__(self, params = LocalizationFilterParams(), secondOrder = False, deltaPosition = False):
        '''
            Creates an instance of the localizationFilter

            input:
                Params      -   Class with filter parameters, should be of type LocalizationFilterParams()
        '''
        
        # Init drone params and rotator-class
        self.__drone_params = DroneGeometry()
        self.__rot = Rot()

        ####### Tools / manipulator classes ########

        self.__histTools = HistogramTools()
        self.__pcTools = PointCloudTools()

        ###########################################

        ######## Likelihood map ########

        self.__likelihood_map = LikelihoodMap()

        ################################

        ############## Const rotations ##############
        self.__rotMat_lf = params.rotMat_lf                     # Rotmat from level body to filter frame
        self.__rotMat_nm = params.rotMat_nm                     # Rotmat from ned to map
        #############################################

        ################### Camera ###################
        self.__max_range = params.max_range                     # Max range reading of the depth sensor [m]
        self.__pos_b_bc = params.pos_b_bc                       # Translation from body frame to cam-frame
        self.__rotMat_bc = params.rotMat_bc                     # Rotmat from camera to body (camera pitch)

        # Sensor model data [in lack of a better name], NOTE: z_hit + z_rand/z_max = 1
        self.__pf_z_hit = params.pf_z_hit       
        self.__pf_z_rand = params.pf_z_rand
        self.__pf_z_max = params.pf_z_max
        ##############################################

        ############ Pointcloud Operations ############
        # Use random points when downsampling
        # Selects nPts randomly, and then checks for duplicates and max_range measurements
        # deletes dupes and max_range measurements from pointcloud before passing on
        self.__pcdsRandPoints = params.pcdsRandPoints

        # Select particles in a loop, checking each point for validity (range, dupe) before adding to an array
        # IF BOTH pcdsLoopSelect AND pcdsRandPoints IS SET TRUE, DEFAULTS TO LOOP CHECK MODE
        self.__pcdsLoopSelect = params.pcdsLoopSelect
        self.__pcdsLoopSelMaxLoops = params.pcdsLoopSelMaxLoops
        ###############################################

        ############### Particle Filter ###############
        # PF Params
        self.__number_of_particles = params.number_of_particles
        self.__init_pose = params.init_pose
        self.__sigma_pose = params.sigma_pose
        self.__nPts_PC = params.nPts_PC                 # Number of points to sample from pointcloud
        self.__pcUpdateSqSum = params.pcUpdateSqSum     # Use Square sum method to update weights from pointcloud data

        # Threshold for resampling (resample if effective sample size is less than threshold)
        self.__resamplingThreshold = params.resamplingThreshold

        # Watchdog variables for when no new velocity message arrives, and the filter keeps predicting
        self.__wd_counter = params.wd_counter
        self.__wd_counter_decayVelocityTresh = params.wd_counter_decayVelocityTresh
        self.__wd_K = params.wd_K
        self.__ekfLinearOnline = True

        # Const variance to add to the pose variance
        self.__constVelVariance = params.constVelVariance

        ### Maximum values ##
        # Max value for velocity std dev
        self.__maxVelStdCtr = params.maxVelStdCtr

        # Create instance of particle filter
        self.__particle_filter = ParticleFilter(    self.__pf_z_hit,
                                                    self.__pf_z_rand, 
                                                    self.__pf_z_max,
                                                    self.__number_of_particles,
                                                    self.__init_pose,
                                                    self.__sigma_pose)

        self.__particle_filter.dry_run(self.__init_pose, self.__sigma_pose, self.__likelihood_map.map)
        ###############################################
        
        # For histogram smoothing
        self.__histogramResolution = params.histogramResolution
        self.__histSmoothingKernel = params.histSmoothingKernel

        # initiate Vectors of Velocities, std.deviations and angles from "outside" of filter
        self.__velVec_l_nb = params.velVec_l
        self.__velStd_l = params.velStd_l

        #### Second order propagation ####
        self.__secondOrder = secondOrder
        if secondOrder == True:
            self.__velVecLast_l_nb = params.velVec_l
            self.__velStdLast_l = params.velStd_l

        self.__tHeta_bl = params.tHeta_bl

        ###### Filter Pose and Variance ######
        self.__pose = self.__init_pose
        self.__poseVariance = self.__sigma_pose**2

        #### Delta position configuration
        self.__deltaPostition = deltaPosition
        if deltaPosition == True:
            self.__lastVelMsgTime = 0.0

        
    # Callers for PF
    def propagate(self, dt):
        '''
            Function to run propogation step of PF
            Rotates velocities from NED to PF propogation frame (Z-Up, X-Forward)

            input:
                dt      -   dt of propogation [float, - unit: s]
        '''

        ### Watch dog stuff ###
        # Increment watch dog counter
        self.__wd_counter += 1

        # If predicts since last velocity update > thresh, decay velocity
        if self.__wd_counter > self.__wd_counter_decayVelocityTresh:
            self.__velVec_l_nb *= (1 - self.__wd_K)
            self.__velStd_l *= (1 - self.__wd_K)

        # Delta position specific configuration
        if self.__deltaPostition == True:
            # If delta position, this converts back to velocity and resets the integrated value
            velVec_l_nb = self.__velVec_l_nb/dt
            self.__velVec_l_nb = np.zeros((4,1))
        else:
            velVec_l_nb = self.__velVec_l_nb

        # Second order specific configuration
        if self.__secondOrder == True:
            # Velocity calculation for second order accuracy
            velVec_l_nb = 1.5*velVec_l_nb - 0.5*self.__velVecLast_l_nb
            self.__velVecLast_l_nb = velVec_l_nb

            # Covariance calculation to reflect second order integration
            velCov_l = 1.5*self.__velStd_l**2 + 0.5*self.__velStdLast_l**2
            velStd_l = np.sqrt(velCov_l)
            self.__velStdLast_l = velStd_l
        else:
            velVec_l_nb = velVec_l_nb
            velStd_l = self.__velStd_l

        # Add value to std.dev from number of predicts since last velocity update, up to a maximum
        # Std dev from message + const + (counter-1)*K
        stdDevFromCtr = np.minimum((self.__wd_counter-1)*self.__wd_K, (self.__maxVelStdCtr), dtype=np.float32)
        propStdDev = velStd_l + np.sqrt(self.__constVelVariance) + stdDevFromCtr


        # Rotate velocities into PF propogation frame (Z-Up, X-Forward)
        vel_PF_nb = np.zeros((4,1))
        vel_PF_nb[0:3,0] = self.__rotMat_lf @ velVec_l_nb[0:3,0]
        vel_PF_nb[3,0] = -velVec_l_nb[3,0]

        # Call propagate method of PF
        self.__particle_filter.propagate(vel_PF_nb, propStdDev, dt)

        if self.__deltaPostition == True:
            self.__velVec_l_nb = np.zeros((4,1))
        
    def pointcloud_update(self, pointcloud):
        ''' 
            Function to run pointcloud update step of PF.
            Downsamples and adjusts pointcloud into level body frame using roll/pitch angles from Kalman Filter.


            input:
                pointcloud      -   Pointcloud from ROS message, converted to np.array(3,N) with [X, Y, Z] along the columns
        '''

        # Downsample pointcloud
        pc_downsampled = self.__pcTools.downsample_pc_arr(  pointcloud,
                                                            max_range       = self.__max_range, 
                                                            nPts            = self.__nPts_PC, 
                                                            randPts         = self.__pcdsRandPoints, 
                                                            loopSelect      = self.__pcdsLoopSelect, 
                                                            maxLoopCount    = self.__pcdsLoopSelMaxLoops) 
        
        # If pointcloud has size 1, no valid points were chosen
        if pc_downsampled.size != 1:
            # Transform pc to level body
            rotMat_bl = self.__drone_params.rotFun_bl(self.__tHeta_bl)
            pc_level = self.__pcTools.transform_pointcloud_to_level_body(   pc_downsampled, 
                                                                            self.__pos_b_bc, 
                                                                            self.__rotMat_bc, 
                                                                            rotMat_bl, 
                                                                            left_hand = False)
            
            # Transform pc to PF frame
            pc_PF = (self.__rotMat_lf @ pc_level).astype(np.float32)

            # Call update
            self.__particle_filter.pointcloud_update(   self.__likelihood_map.map,
                                                        self.__likelihood_map.origin_offset, 
                                                        self.__likelihood_map.resolution,
                                                        self.__likelihood_map.size_in_cells,
                                                        self.__likelihood_map.mapMaxGaussVal,
                                                        self.__likelihood_map.mapUsingUint8Prob,
                                                        pc_PF,
                                                        self.__pcUpdateSqSum)
            
            # Resample
            if self.getPFEffectiveSampleSize() < self.__resamplingThreshold: 

                self.__particle_filter.systematicResample()

    # Histogram smoothing and localization
    def localize(self):
        '''
            Function to find most likely localization from PF and its variance
            Uses kernel smoothing of pose histograms using a gaussian kernel
            Saves pose and pose variance to filter variables
        '''
        
        # Get histograms
        histogramX, histogramY, histogramZ, histogramPsi = self.__createPoseHistograms()

        # Smooth histograms
        histX, binsX = self.__histTools.smoothPoseHistogram(histogramX, self.__histSmoothingKernel, False)
        histY, binsY = self.__histTools.smoothPoseHistogram(histogramY, self.__histSmoothingKernel, False)
        histZ, binsZ = self.__histTools.smoothPoseHistogram(histogramZ, self.__histSmoothingKernel, False)
        histPsi, binsPsi = self.__histTools.smoothPoseHistogram(histogramPsi, self.__histSmoothingKernel, True)

        # Get indexes of max values from histograms
        histXMaxIdx = np.argmax(histX)
        histYMaxIdx = np.argmax(histY)
        histZMaxIdx = np.argmax(histZ)
        histPsiMaxIdx = np.argmax(histPsi)

        # Take average of associated bin edges
        xPose = (binsX[histXMaxIdx] + binsX[histXMaxIdx + 1])/2.0
        yPose = (binsY[histYMaxIdx] + binsY[histYMaxIdx + 1])/2.0
        zPose = (binsZ[histZMaxIdx] + binsZ[histZMaxIdx + 1])/2.0
        psiPose = (binsPsi[histPsiMaxIdx] + binsPsi[histPsiMaxIdx + 1])/2.0

        # Collect most likely pose into an array
        pose = np.array([[xPose],[yPose],[zPose],[psiPose]], dtype = np.float32)

        # Find variance from pose
        poseVariance = self.__particle_filter.computePoseVariance(pose)

        # Set to filter variables, add some const variance to the poseVariance
        self.__pose = pose
        self.__poseVariance = poseVariance
          
    # Histogram func
    def __createPoseHistograms(self):
        '''
            Function to get histograms of the poses in the different directions, 
            ** Not in separate JIT-class as numba (0.50) does not allow for weighted histograms
        '''
        
        # Get pose and weight vector from particle filter
        poseVec = self.__particle_filter.getParticlePoseVector()
        weightVec = self.__particle_filter.getParticleWeightVector()

        # Find number of bins needed to get the wanted resolution
        nBins = np.ones((4,1), dtype = np.int32)
        for ii in range(nBins.shape[0]):
            # Calculate number of bins for current axis from max and min value of poseVec
            nBins[ii,0] = np.int32(np.round_(((poseVec[ii,:].max() - poseVec[ii,:].min())/self.__histogramResolution[ii,0]) , decimals=0))

            if nBins[ii,0] < 1:
                # If number of bins less than one, add one to not break histogram
                # should only happen if data has essentially no spread, where one bin would be the correct amount
                nBins[ii,0] = 1

        # Create histograms        
        histogramX = np.histogram(poseVec[0,:], bins = nBins[0,0], weights = weightVec[0,:])
        histogramY = np.histogram(poseVec[1,:], bins = nBins[1,0], weights = weightVec[0,:])
        histogramZ = np.histogram(poseVec[2,:], bins = nBins[2,0], weights = weightVec[0,:])
        histogramPsi = np.histogram(poseVec[3,:], bins = nBins[3,0], weights = weightVec[0,:])

        # Return histograms
        return histogramX, histogramY, histogramZ, histogramPsi
 
    # Reset func
    def resetParticleFilterToInitPose(self):
        '''
            Function to reset Particle filter to initial pose
        '''
        # Call PF resetter func with initial pose 
        self.__particle_filter.reset_filter(self.__init_pose, self.__sigma_pose)

    # Public setters
    def setEKFLinearOnlineState(self, status):
        # Sets status of ekf
        self.__ekfLinearOnline = status

    def setPFParams(self, params):
        # Method to set params to PF from params "struct"
        test = 0

    def setHistogramSmoothingKernel(self, kernel):
        # Set kernel for histogram smoothing
        self.__histSmoothingKernel = kernel

    def setPropagationVelocityWithCovariance(self, vel_vec, cov_vec, time):
        '''
            Set velocity to be used in particle propogation, velocity should be in level body frame, [x, y, z, Psi] column vector
            Set velocity standard deviation to be used in particle propogation, cov should be in level body frame, [x, y, z, Psi] column vector

            input:
                vel_vec     -   [4x1] Vector of floats, containing velocities in [x, y, z, Psi] in level frame
                cov_vec     -   [4x1] Vector of floats, containing cov in [x, y, z, Psi] in level frame
        '''
        
        if self.__deltaPostition == True:
            # Calculates msg dt
            # gets time
            dt = time - self.__lastVelMsgTime
            self.__lastVelMsgTime = time
            if dt >= 1.0:
                dt = 0.0
                print('wrn: PF_Ros_noe: deltaPose dt not valid')

            self.__velVec_l_nb += vel_vec*dt
        else:
            self.__velVec_l_nb = vel_vec

        # Sets std
        self.__velStd_l = np.sqrt(cov_vec)

        # Resets wd counter if linear part of EKF is online
        if self.__ekfLinearOnline:
            self.__wd_counter = 0

    def setPropagationStdDev(self, std_vec):
        '''
            Set velocity standard deviation to be used in particle propogation, std.dev should be in level body frame, [x, y, z, Psi] column vector

            input:
                std_vec     -   [4x1] Vector of floats, containing std.dev in [x, y, z, Psi] in level frame
        '''
        self.__velStd_l = std_vec

    def setCurrentRollPitchAngles(self, tHeta_bl):
        '''
            Sets angles tHeta_bl (from body to level) shape [3,1], (Roll, Pitch, Yaw) to filter
        '''
        self.__tHeta_bl = tHeta_bl

    # Public getters
    def getPFEffectiveSampleSize(self):
        # Return PF effective sample size
        return self.__particle_filter.getEffectiveSampleSize()

    def getPFParticlePoseVector(self):
        return self.__particle_filter.getParticlePoseVector()

    def getPFParticleWeightVector(self):
        return self.__particle_filter.getParticleWeightVector()

    def getFilterPoseNED(self):
        # Convert to NED frame
        pose_n_nb = np.zeros((4,1))
        pose_n_nb[:3,0] = self.__rotMat_nm.T @ self.__pose[:3,0]
        pose_n_nb[3,0] =  (np.pi/2 - self.__pose[3,0]) % (2.0*np.pi)

        return pose_n_nb.astype(np.float32)

    def getFilterVarianceNED(self):

        # Convert to NED frame, X- and Y- changes place
        nedVar = np.zeros((4,1), dtype=np.float32)
        nedVar[0,0] = self.__poseVariance[1,0]
        nedVar[1,0] = self.__poseVariance[0,0]
        nedVar[2:,0] = self.__poseVariance[2:,0]

        return nedVar
