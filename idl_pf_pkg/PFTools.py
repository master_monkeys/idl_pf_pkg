# Other imports
import numpy as np

# Numba 
from numba import int32, float32, jit, types, typed, typeof  # import the types
from numba.experimental import jitclass


@jitclass([])
class HistogramTools():
    '''
        Class containing methods to manipulate histograms
    '''
    def __init__(self):
        # Dry run to test functions
        self.dry_run()
        
    def dry_run(self):
        # Test params
        testPose = np.ones((1,10), dtype = np.float32)
        testPose[0,5] = np.float32(2.0)
        testKernel = np.ones((1,3),dtype = np.float32)

        # Run funcs
        hX = np.histogram(testPose)
        test1, test2 = self.smoothPoseHistogram(hX, testKernel, False)

    def smoothPoseHistogram(self, hist, kernel, wrapped = False):
        '''
            Function to smooth histogram

            inputs:
                hist        -   Histogram, [hist, bins] as given from hist = np.histogram()
                kernel      -   Kernel to smooth with, should be a np.array of size [1xN] where N is odd
                wrapped     -   True/False if the histogram is wrapped (i.e 0 - 2pi)

            output:
                smoothedHist-   smoothed histogram
                binEdges    -   Edges of the bins in the histogram
        '''

        # Get histogram and bin edges
        histogram = hist[0].reshape((1, hist[0].shape[0]))
        binEdges = hist[1]

        # Get length of histogram & kernel
        histLen = np.int32(histogram.shape[1])
        kernelLen = np.int32(kernel.shape[1]) 

        kernelPad = kernelLen - 1       # Odd kernel length -> (kernelLen - 1)/2 extra on each side

        # Init smoothed and padded hist as zeros
        smoothedHist = np.zeros((1, histLen), dtype=np.float32)
        paddedHist = np.zeros((1, histLen + kernelPad), dtype = np.float32)

        # Find offset from kernel size
        kernelOffset = np.int32((kernelPad)/2)

        # insert histogram into padded histogram
        paddedHist[0, kernelOffset:histLen + kernelOffset] = histogram[0,:]

        # If wrapped 0...2pi and histogram endpoints are within some % of edge values, 
        # pad histogram with values on opposite ends of original histogram
        if wrapped:

            # Calculate thresholds for wrapping
            yawWrapWindow = 0.05                         # 5 % of 2*pi
            upperThresh = np.pi*2.0*(1-yawWrapWindow)
            lowerThresh = np.pi*2.0*yawWrapWindow

            if binEdges.max() > upperThresh and binEdges.min() < lowerThresh:
                # If extreme-values of histogram are within a certain threshold of % 2pi, pad with values from opposite side of histogram
                paddedHist[0, 0:kernelOffset] = histogram[0, -kernelOffset:]
                paddedHist[0, -kernelOffset:] = histogram[0, 0:kernelOffset]


        # For loop to dot kernel with part of histogram
        for ii in range(histLen):
            # Get slice of histogram data
            histData = (paddedHist[0,ii:ii+kernelLen]).reshape(1,kernelLen)

            kernel = kernel

            # Dot product between kernal and histogram slice
            smoothedHist[0,ii] = (kernel*histData).sum()

        return smoothedHist, binEdges

class PointCloudTools():
    '''
        Class containing methods to manipulate pointclouds
    '''
    def __init__(self):
        # Default contructor

        # Run dry_run func
        self.dry_run()

    def dry_run(self):
        # Dry running funcs
        pc_test = np.ones((1,3))


        # Run funcs
        dry_run_downsample = self.downsample_pc_arr(pc_test)
        test = self.transform_pointcloud_to_level_body(dry_run_downsample)

    def downsample_pc_arr(self, pc_arr, max_range=15.0, nPts=0, randPts=False, loopSelect=False, maxLoopCount=None):
        '''
            Input:
                pc_arr          -   np.array with pointcloud data
                nPts            -   Number of points from the cloud to "keep" (linspaced through all points which are not NaN) 0 keeps all points
                randPts         -   Picks points randomly using a uniform distribution
                loopSelect      -   Bool to signify that we want to use a loop to check for valid points
                maxLoopCount    -   Maximum number of times to loop when finding points using loopSelect

            Output:
                pointcloud_filtered     -   Np array with coordinated of all points in PC, size: (3 x n)
                                            where n is amount of points in PC

        '''

        # Initialize data to 0
        pointcloud_filtered = 0

        # Init bool to signify if the values are verified before reaching the end, if the
        # values are verified, then you won't need to loop and check for uniqueness and that dist < max_range
        isChecked = False

        # If loopSelect is true and maxLoopCount not set, set maxLoopCount equal to nPts
        if loopSelect and (maxLoopCount is None):
            maxLoopCount = nPts

        # Only run if there are valid points in xyz_data
        if pc_arr.size != 0:
            if nPts == 0:
                # If input nPts to use is zero, use entire pointcloud
                pointcloud_filtered = pc_arr

            elif not randPts and not loopSelect:
                # Else if randPts is not set to true, use linspace to pick points
                indexes = np.linspace(0, pc_arr.shape[0], nPts, endpoint=False, retstep=False, dtype=np.int32)
                pointcloud_filtered = pc_arr[indexes,:]

            elif loopSelect:

                # Init vector of indexes
                idxVec = np.empty(0, dtype=np.int32)
                loopCounter = 0

                # Loop untill while is broken by either:
                # Enough points are found [idxVec.shape[0] < nPts], or
                # The number of tested points exceed maxLoopCount [loopCounter < maxLoopCount]
                while (loopCounter < maxLoopCount) and (idxVec.shape[0] < nPts):
                    # Increment counter
                    loopCounter += 1
                    
                    # Get a random index from the cloud
                    idx = np.random.randint(0, pc_arr.shape[0], dtype=np.int32)

                    # Get point from cloud
                    point = pc_arr[idx,:]

                    # Check if closer than max-range
                    dist = np.sqrt(np.sum(point**2))

                    if dist < max_range:
                        # Check if idx is in idxVec
                        isIn = np.isin(idx, idxVec)

                        # If it's not in, add to idxVec
                        if not isIn:
                            idxVec = np.append(idxVec, idx)
                
                # Set isChecked True, as all indexes are unique and closer than max_range
                isChecked = True

                # Extract values
                pointcloud_filtered = pc_arr[idxVec,:]   


            else: 
                # Get nPts randomly selected points from PC array
                randIdx = np.random.randint(0, pc_arr.shape[0], nPts)

                # Ensure only unique points selected
                randIdx = np.unique(randIdx)

                # Slice array to get points
                pointcloud_filtered = pc_arr[randIdx,:]


            if isChecked is False:
                # Initialize list to keep indexes of points at max range
                points_at_max_range = []

                for ii in range(pointcloud_filtered.shape[0]):
                    # Check if reading is at max dist
                    point_dist = np.sqrt(np.sum(pointcloud_filtered[ii,:]**2))

                    # If distance is greater than max range
                    if point_dist > max_range:
                        points_at_max_range.append(ii)

                # Delete max range readings
                # this simply removes the max range points, meaning the pointcloud is no longer nPts big
                pointcloud_filtered = np.delete(pointcloud_filtered, points_at_max_range, axis=0)


        # Return transposed data, to get a [3xN] list 
        return np.transpose(pointcloud_filtered).astype(np.float32)

    def transform_pointcloud_to_level_body(self, pointcloud_array, pos_b_bc=np.zeros((3,1)), rotMat_bc=np.eye(3), rotMat_bl=np.eye(3), left_hand=False):
        '''
            Input:
                pointcloud_array        -   pointcloud in np.array form [x, y, z]... camera frame (Z-Forward, X-Down)
                pos_b_bc                -   translation from body to camera [x_t, y_t, z_t]
                rotMat_bc               -   Rotation matrix from body to camera
                rotMat_bl               -   Rotation matrix from body to level
                left_hand               -   if the coordinate frame is lefthanded

            Output:
                pointcloud_transformed  -   pointcloud transformed into level body frame [X-Forward, Z-Down]

            Function to transform pointcloud into "artificially level body frame" where the roll and pitch angle from the
            kalman filter is used to straighten up the pointcloud for use with the map.

        '''
        if left_hand:
            # If lefthanded coordinate system, flip X-Coordinates
            #print(pointcloud_array[0][:])
            pointcloud_array[0][:] = -pointcloud_array[0][:]

        # Rotate pointcloud into body frame [X-Forward, Z-Down]
        pointcloud_rotated = rotMat_bc @ pointcloud_array

        # Translate pointcloud from camera frame to level frame
        pointcloud_transformed = rotMat_bl @ (pos_b_bc + pointcloud_rotated)

        return pointcloud_transformed.astype(np.float32)

