import numpy as np




def coordinateTransform(theta):
    return np.array([[np.cos(theta), -np.sin(theta), 0.0, 0.0],
                     [np.sin(theta), np.cos(theta), 0.0, 0.0],
                     [0.0, 0.0, 1.0, 0.0],
                     [0.0, 0.0, 0.0, 1.0]],dtype=np.float32)


class Particle(object):

    def __init__(self, pose, initWeight):
        # Particle pose
        self.pose = pose
        self.weight = initWeight

    def move(self, V, dt):
        # Rotation about z
        R = coordinateTransform(self.pose[3][0]+ V[3][0]*dt/2.0)

        # Update pose
        self.pose += R @ (V*dt).astype(np.float32)

        # Wrap yaw 0 - 2*pi
        self.pose[3] = np.mod(self.pose[3], 2.0*np.pi)
   

class ParticleFilter(object):
    
    def __init__(self, z_hit, z_rand, z_max, nParticles, initPose, sig_pose):
        '''
            Creates a Particle Filter object

            input:
                z_hit, z_rand, z_max,   :   Sensor parameters                               (floats)
                nParticles              :   Number of particles in filter                   (int)
                initPose                :   Initial Pose of Particles                       ((4x1) vector, [x, y, z, yaw])
                sig_pose                :   Std deviation of particles around initial pose  ((4x1) vector, [x, y, z, yaw])

        '''
        # Parameters for scan matching
        self.__z_hit = z_hit
        self.__z_rand = z_rand
        self.__z_max = z_max 

        # Create list of particles
        self.__nParticles = nParticles                                          # Number of particles in filter
        self.__particles = typed.List.empty_list(particleNumbaType)             # List of particle objects
        self.__effectiveParticles = nParticles                                  # Effective sample size of the particle filter (measure of degeneracy)
        self.__poseVec = np.zeros((4,self.__nParticles), dtype = np.float32)    # Vector containing particle poses
        self.__weightVec = np.zeros((1, self.__nParticles), dtype = np.float64) # Vector containing particle weights

        # Uniform initial weight for all particles
        initWeight = np.float32(1.0/nParticles)

        for ii in range(nParticles):
            # Add random noise to initial pose with std.dev sig_pose
            initPose_with_noise = self.normalDistVector(initPose, sig_pose)
            self.__particles.append(Particle(initPose_with_noise, initWeight)) 
            self.__poseVec[:,ii] = self.__particles[ii].pose.copy().reshape(4)
            self.__weightVec[0,ii] = initWeight

        print("Particle filter object created!")

    # Function to reset all particles to have pose around [pose] with std.dev [sig_pose]
    def reset_filter(self, pose, sig_pose):
        '''
            Method to reset filter to a chosen pose with a set std deviation
        '''

        # Calculate normalized uniform weight
        particleWeight = np.float32(1.0/self.__nParticles)

        # Reset all particles
        for ii in range(self.__nParticles):
            # Add random noise to pose with std.dev sig_pose
            initPose_with_noise = self.normalDistVector(pose, sig_pose)
            self.__particles[ii].pose = initPose_with_noise.copy()
            self.__particles[ii].weight = particleWeight*1.0
            self.__poseVec[:,ii] = self.__particles[ii].pose.copy().reshape(4)
            self.__weightVec[0,ii] = particleWeight
     
    # Function to dry-run all filter methods, to not have to JIT-compile during operation   
    def dry_run(self, initPose, sig_pose, likelihoodMap):
        '''
            Does a dry run of all functions in the filter, only really used in the JIT-compiled version to have all functions in the
            class compile at init to avoid long delays during runtime.
        '''
                 
        # For propogation dry-run 
        dry_run_vector4 = np.ones((4,1), dtype=np.float32)             # 3x1 vector of zeros to pass into filter functions
        dry_run_dt = 0.0001

        # For update
        dry_run_map = likelihoodMap
        dry_run_map_origin_offset = np.zeros((3,1), dtype = np.float32) 
        dry_run_map_resolution = np.float32(0.1)
        dry_run_map_size_in_cells = np.ones((3,1), dtype = np.int32)
        dry_run_pointcloud = np.ones((3,100), dtype = np.float32)
        
        # Call functions in filter
        self.propagate(dry_run_vector4, dry_run_vector4, dry_run_dt)

        self.normalDistVector(dry_run_vector4, dry_run_vector4)
        
        # Pointcloud Update
        self.pointcloud_update(dry_run_map,
                               dry_run_map_origin_offset,
                               dry_run_map_resolution,
                               dry_run_map_size_in_cells,
                               1.0,
                               False,
                               dry_run_pointcloud,
                               False)
        
        # Resampling functs
        self.getEffectiveSampleSize()
        self.systematicResample()

        # Normalize
        self.normalize_particle_weight(1.0)

        # Reset
        self.reset_filter(initPose, sig_pose)


    # Function to create a [4x1] vector drawn from a gaussian distribution around vec_mu, with ve_sig std.dev
    def normalDistVector(self, vec_Mu, vec_Sig):
        '''
            Since numba does not support np.random.normal with vector inputs, we make our own
        '''
        return np.array([[np.random.normal(vec_Mu[0][0], vec_Sig[0][0])],
                         [np.random.normal(vec_Mu[1][0], vec_Sig[1][0])],
                         [np.random.normal(vec_Mu[2][0], vec_Sig[2][0])],
                         [np.random.normal(vec_Mu[3][0], vec_Sig[3][0])]],dtype=np.float32) 

    # Function to propagate the particles
    def propagate(self, V, sig_v, dt):
        '''
            Function that propogates the particles in space
            according to the motion model in particle class

            TODO: Move on from function-based propogation
        '''

        # Make sure data is in correct format
        V = V.astype(np.float32)
        sig_v = sig_v.astype(np.float32)
        dt = np.float32(dt)

        # Propagate particles
        for particle in self.__particles:
            # Add some random noise to propogation velocity
            V_with_noise = self.normalDistVector(V, sig_v)

            # Propogate particles
            particle.move(V_with_noise, dt)
    
    # Function to resample low-weight particles
    def systematicResample(self): 
        '''
            Systematic resampling (Page 5, Table 2, Code block 3: https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7079001&tag=1)
        '''
        # Draw random number between 0 and 1/nParticles as initial upper limit for cumulative sum of weight
        r = np.random.uniform(0, 1/self.__nParticles)
        
        # Initiate cumulative sum of weights as weight of first particle
        W = self.__particles[0].weight
        highestWeightIndex = 0
        highestWeight = W*1.0

        # Initiate indexing variable
        # Used to index particles to replicate in the loop
        i = 0

        for n in range(self.__nParticles):
            # Add 1/N to upper limit of cumulative sum of weights
            u = r + (n)/self.__nParticles

            # While cumulative sum of weights is lower than u
            while W < u:
                # Increment indexing variable
                i += 1

                # If i exceeds range of list, set i = last index to not index outside of bounds, and set W to u to break loop
                if i >= self.__nParticles:
                    i = self.__nParticles - 1
                    W = u

                # Update cumulative sum of weights with weight of particle at index i
                W += self.__particles[i].weight


            # Replace particle pose at index n with particle pose at index i
            # multiply with 1.0 to break reference with self.__particles[i].pose
            # omitting *1.0 makes the pose referenced, and any change to particle[i]'s pose
            # will be the same in all particles referenced.
            self.__particles[n].pose = np.copy(self.__particles[i].pose)

            # Give all particle weight 1/N
            self.__particles[n].weight = np.float32(1.0/self.__nParticles)
          
    # Function to compute the variance of the pose
    def computePoseVariance(self, pose):
        '''
            Compute variance of all particles from a given particle pose
        '''

        sqError = np.zeros((4,1), dtype=np.float32)
        V2 = 0
        V1 = 0
        
        # For loop through and add square error
        for ii in range(self.__nParticles):
            # Cumulative sum of square errors

            # Positions
            sqError[:3,0] += self.__particles[ii].weight*(self.__particles[ii].pose[:3,0] - pose[:3,0])**2
            
            # Yaw, make sure to pick shortest dist e
            yawError = self.__innovationYaw(self.__particles[ii].pose[3,0], pose[3,0])

            sqError[3,0] += self.__particles[ii].weight*(yawError**2)

            # Sum up V1 and V2
            V1 += self.__particles[ii].weight
            V2 += self.__particles[ii].weight**2
                    

        ### Find vaiance ###
        # var = V1/(V1^2 - V2) * sqError
        # Helper variables
        num = V1
        denom = V1**2 - V2

        # Minimum value for denominator in variance calculation
        minDenom = 0.00001

        # Make sure denom does not get too small
        if denom < minDenom:
           denom = minDenom
           print("Denominator in variace calc too small, setting minimum value")

        # Calculate weighted variance
        varPose = (num/denom)*sqError

        return varPose.astype(np.float32)

    # Function to find shortest "geodesic" distance around circle (for yaw)
    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi mapping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        return e

    # Function to update weights of particles using measured pointcloud
    def pointcloud_update(self, likelihood_map, map_origin_offset, map_resolution, map_size_in_cells, mapMaxGaussVal, mapUsingUint8Prob, pointcloud_map_frame_body_centered, sqSum=False):
        '''
            Function to transform pointcloud into particle frames and check against map
            Updates weight as product of all map hit probabilities

            TODO:   Implement different weight update if i find the source
        '''
        # for all particles:
        #   rotate and translate san into particle frame
        #   convert scan coordinates to indexes to get prob. from map
        #   check that all indices are valid
        #   update weight of all particles
        #   normalize weights

        # Initialize sum of weights to zero, used in normalizing step
        sum_weights = np.float64(0.0)

        # Calculate z_rand/z_max before loop
        z_misc = self.__z_rand/self.__z_max

        # initialize factor to multiply map prob with, gives possibility to use more data-types
        mapProbabilityFactor = np.float64(1.0)

        # if map uses uint8 as probabilities
        if mapUsingUint8Prob:
            # Update factor
            mapProbabilityFactor = np.float64(mapMaxGaussVal) / (np.float64(255.0))

        for ii in range(self.__nParticles):
            # Get rotation-matrix from particle to map:
            rotmat_pm = coordinateTransform(self.__particles[ii].pose[3,0])[:3,:3]

            # Rotate pointcloud into particle frame
            pointcloud_particle_frame = rotmat_pm @ pointcloud_map_frame_body_centered

            # Translate pointcloud to particle position
            pointcloud_map_frame = self.__particles[ii].pose[:3].copy() + pointcloud_particle_frame

            # Offset the pointcloud and round to find map indices, then cast to int32
            indices = np.empty_like(pointcloud_map_frame)
            np.round_((pointcloud_map_frame - map_origin_offset)/map_resolution, 0, indices)
            indices = indices.astype(np.int32)


            ###### Check validity of points ######
            # Find all points where there are no negative indices
            positive_indices_logical_raw = (indices >= 0) # Check if indices (x-, y- and z-) are positive [True / False]
            # Messy because JIT does not allow .all() with defined axis, the following ANDs along the column of the vector
            positive_indices_logical = (positive_indices_logical_raw[0,:]*positive_indices_logical_raw[1,:]*positive_indices_logical_raw[2,:]) # And through each column to check validity of point

            # Check if in map
            indices_in_map_logical_raw = (indices < map_size_in_cells.copy().reshape((3,1))) # Check if coordinates (x-, y- and z-) are inside map [True / False]
            #Messy because JIT does not allow .all() with defined axis
            indices_in_map_logical = (indices_in_map_logical_raw[0,:]*indices_in_map_logical_raw[1,:]*indices_in_map_logical_raw[2,:]) # And through each column to check validity of point
            #indices_in_map = indices_in_map_logical.nonzero()[0] # Find index of point

            # AND the vectors element-wise to find valid points, then get indices of all non-zero (True) values
            valid_point_indexes = (indices_in_map_logical*positive_indices_logical).nonzero()[0] # Get index of points that are both positive AND inside map


            #### Get probabilities from map ####
            # initialize list of probabilities to z_misc 
            probabilities_from_points = np.ones(indices.shape[1], dtype=np.float64)*z_misc

            # Loop only through valid points
            for jj, point_index in enumerate(valid_point_indexes):
                # Get probability from map
                probFromMap = likelihood_map[indices[0,point_index], indices[1,point_index], indices[2,point_index]]

                # Multiply with probability factor and z_hit
                probabilities_from_points[point_index] = mapProbabilityFactor * self.__z_hit * np.float64(probFromMap) 
                
                # Add z_misc
                probabilities_from_points[point_index] += z_misc
            

            # If sqSum, set prob from map as: sum(prob_i^2)/nPts_PC
            if sqSum:
                probability_from_map = (probabilities_from_points**2).sum()/indices.shape[1]
            else:
                # Find probability by taking product of all probabilities in array
                probability_from_map = probabilities_from_points.prod()

            # Take product of probabilities from map and assign weight to particle
            self.__particles[ii].weight *= probability_from_map

            # Add to sum of weight for normalization
            sum_weights += self.__particles[ii].weight

        
        # Normalize weights, also sets effective particles       
        self.normalize_particle_weight(sum_weights)

    # Function to normalize particle weights
    def normalize_particle_weight(self, sum_weights):
        # Normalize particle weights and update effective particle set
        # initialize sum of squared weights
        sumSquaredWeights = 0

        for ii in range(self.__nParticles):
            # Normalize particle weights
            self.__particles[ii].weight = self.__particles[ii].weight/sum_weights
            #print(self.__particles[ii].weight)
            
            # Keep track of sum of squared weights (Neff = 1/(sum(weights^2)))
            sumSquaredWeights += self.__particles[ii].weight**2

            # Update vector with particle poses & weights
            self.__poseVec[:,ii] = self.__particles[ii].pose.copy().reshape(4)
            self.__weightVec[0, ii] = self.__particles[ii].weight

            # Print for debugging.
            #print(self.__particles[ii].weight)

        # Get effective number of particles
        self.__effectiveParticles = 1.0/(sumSquaredWeights)

    # Func to print particle position
    def printParticlePos(self):
        '''
            Function to Print particle positions in terminal
            Only used fo simplicity.

        '''
        for i in range(len(self.__particles)):
            print("Particle has pose: " )
            print(self.__particles[i].pose)
            print(" ")
    
    # Getters
    def getParticlePoseVector(self):
        return self.__poseVec.copy()

    def getParticleWeightVector(self):
        return self.__weightVec.copy()
      
    def getEffectiveSampleSize(self): 
        '''
            Returns number of effective particles
        '''
        return self.__effectiveParticles

  