# Other imports
import numpy as np

# Own stuff
# Pf filter stuff
from idl_pf_pkg.JitParticleFilterClass import *
from idl_pf_pkg.LocalizationFilter import *
# Geometric data
from idl_botsy_pkg.droneConfiguration import DroneGeometry, Rot

# Filter Specifications
from idl_botsy_pkg.filterConfig import FilterInitialStates, FilterConfiguration, ParticleFilterSetup
from idl_botsy_pkg.softwareConfigutarion import *

# ROS stuff
import rclpy
import ros2_numpy as rnp
import ros2_numpy.point_cloud2 as pc2
import ament_index_python
from std_msgs.msg import Bool
from sensor_msgs.msg import PointCloud2, PointCloud, ChannelFloat32
from geometry_msgs.msg import PoseWithCovarianceStamped, TwistWithCovarianceStamped, Point32
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from rclpy.time import Time
        
class PF_ros_node(Node):
    '''
        ROS2 Particle filter node
        Subscribers and publishers are defined at the bottom of __init__ so as to not queue a lot of messages as the JIT class compiles
    '''

    def __init__(self):
        super().__init__('PF_ros_node')

        # Filter configuration class
        filterConfig = FilterConfiguration()

        # Filter initial values
        initStatesNED = FilterInitialStates()

        # Particle filter setup params
        pfSetup = ParticleFilterSetup()

        # Dronegeom for rotation matrices etc
        droneGeom = DroneGeometry()
        
        # filterConfig.gazeboGT, for defining if you're going to be using velocity and roll/pitch data
        # from groundTruth publisher or from Kalman Filter, set in config file to "synchronize" filters

        # Set message source
        if filterConfig.gazeboGT:
            messageSource = 'gazeboGT/'

        else:
            messageSource = 'ekf/'

        #### Filter params ####
        filter_params                       = LocalizationFilterParams()

        ### Sensor model Params ###
        # Sensor model data [in lack of a better name], NOTE: z_hit + z_rand/z_max = 1
        filter_params.pf_z_hit              = pfSetup.pf_z_hit         
        filter_params.pf_z_rand             = pfSetup.pf_z_rand
        filter_params.pf_z_max              = pfSetup.pf_z_max
        filter_params.max_range             = pfSetup.pf_sensMaxRange

        ### Particle Filter ###
        # PF Params
        filter_params.number_of_particles   = pfSetup.numParticles
        
        # Init position for particle generation
        initPos                             = droneGeom.rotMat_nm @ initStatesNED.pos.asNpArray()
        initPsi                             = np.pi/2 - initStatesNED.tHeta.z
        filter_params.init_pose             = np.array([[initPos[0,0]],
                                                        [initPos[1,0]],
                                                        [initPos[2,0]],
                                                        [initPsi] ],dtype=np.float32)
        
        # Covariance for initial particle spread
        initPosCovNED                       = initStatesNED.posCov # In NED frame, switch X and Y to get into map frame
        inittHetaCovNED                     = initStatesNED.tHetaCov
        filter_params.sigma_pose            = np.array([[initPosCovNED.y],
                                                        [initPosCovNED.x],
                                                        [initPosCovNED.z],
                                                        [inittHetaCovNED.z]],dtype=np.float32)

        # Number of points to use from pointcloud in update step
        filter_params.nPts_PC               = pfSetup.nPts_PC   # Number of points to sample from pointcloud

        # Wether or not to use the square sum method to update the weight in the pointcloud-update step
        filter_params.pcUpdateSqSum         = pfSetup.pcUpdateSqSum

        # PC downsampling configuration
        filter_params.pcdsRandPoints = pfSetup.pcdsRandPoints
        filter_params.pcdsLoopSelect = pfSetup.pcdsLoopSelect
        filter_params.pcdsLoopSelMaxLoops = pfSetup.pcdsLoopSelMaxLoops

        # Threshold for resampling 
        # (resample if effective sample size is less than threshold)
        filter_params.resamplingThreshold   = pfSetup.resamplingThreshold

        ### Propogation velocity ###
        # Const cov to be added to propogation velocity
        filter_params.constVelVariance      = np.array([[pfSetup.constVar.x],
                                                        [pfSetup.constVar.y],
                                                        [pfSetup.constVar.z],
                                                        [pfSetup.constVarPsi]], dtype = np.float32)
        
        # Max value of added velocity std.dev when no new velocity message received
        maxVelStdCtr                        = pfSetup.maxVelStdCtr
        filter_params.maxVelStdCtr          = np.array([[maxVelStdCtr],
                                                        [maxVelStdCtr],
                                                        [maxVelStdCtr],
                                                        [maxVelStdCtr]], dtype = np.float32)

        # Watchdog counter gain K for velocity std.dev 
        filter_params.wd_K                  = pfSetup.wd_K

        # Init propagation noise
        filter_params.velStd_l              = np.array([[initStatesNED.linVelCov.y],
                                                        [initStatesNED.linVelCov.x],
                                                        [initStatesNED.linVelCov.z],
                                                        [initStatesNED.angVelCov.z]], dtype = np.float32)

        ### Histogram Smoothing ###
        histRes                             = pfSetup.histRes #  0.01 [m], 0.01 [rad] (~ 0.57 degrees) 
        smoothingGaussianStdDev             = pfSetup.histGaussStdDev
        filter_params.histogramResolution   = np.array([[histRes],
                                                        [histRes],
                                                        [histRes],
                                                        [histRes]], dtype=np.float32)
        histKernel                          = filter_params.computeGaussianKernel(histRes, smoothingGaussianStdDev)
        filter_params.histSmoothingKernel   = histKernel

        
        ##### Init filter #####
        self.get_logger().info("Filter init start...")
        self.__localizationFilter           = LocalizationFilter(params = filter_params, 
                                                                 secondOrder = pfSetup.secondOrderIntegration,
                                                                 deltaPosition = pfSetup.deltaPositionIntegration)
        self.get_logger().info("Filter init Completed!")

        # Indicator to be set when kf is ready, initiates propagation
        self.__systemReadyIndicator         = True
        

        ##### Particle pointcloud #####
        # Bool to disable publishing if desired
        self.__publishPFParticlePointcloud  = pfSetup.pubPFParticlePC

        # Rate of which to publish pointcloud
        pfPointcloudPublisherRate           = pfSetup.pfPCPublisherRate
        pfPointcloudPublisherDt             = 1.0 / pfPointcloudPublisherRate
        
        # Publish poitcloud containing __pfPointcloudSize number of particles
        self.__pfPointcloudSize             = pfSetup.pfPCSize

        ##### Timers #####
        ## Rates ##
        propogation_rate                    = 10
        localization_rate                   = 10

        ## dt ##
        self.prop_dt                        = 1.0/propogation_rate
        localization_dt                     = 1.0/localization_rate

        ## Timers ##
        self.__PF_propogation_timer         = self.create_timer(self.prop_dt, self.__propogation_callback)
        self.__localization_timer           = self.create_timer(localization_dt, self.__localization_callback)
        
        if self.__publishPFParticlePointcloud:
            # Only create timer if bool is set
            self.__PFPCPublishTimer         = self.create_timer(pfPointcloudPublisherDt, self.__publish_particlePointCloud)

        ## Init subscribers ##
        if simulation is True:
            self.__pointcloud_subscriber    = self.create_subscription(PointCloud2, '/zed_mini_depth/points', self.__pointcloud_callback, 10)

        else:
            
            self.__pointcloud_subscriber    = self.create_subscription(PointCloud2, '/zedm/zed_node/point_cloud/cloud_registered', self.__pointcloud_callback, 10)


        self.__velocity_subcriber           = self.create_subscription(TwistWithCovarianceStamped, messageSource + 'vel_level', self.__velocity_callback, 10)
        
        self.__rollPitch_subcriber          = self.create_subscription(PoseWithCovarianceStamped, messageSource + 'pose_ned', self.__roll_pitch_callback, 10)

        self.__systemReset_subscriber       = self.create_subscription(Bool, 'ins/system/reset', self.__systemReset_callback, 10)

        self.__systemStart_subscriber       = self.create_subscription(Bool, 'ins/system/start', self.__systemStart_callback, 10)

        ## Publisher to publish position and yaw ##
        self.__pose_publisher               = self.create_publisher(PoseWithCovarianceStamped, 'pf/pose_ned', 10)

        ## Publisher to publish pointcloud of particle positions ##
        self.__pose_pointcloud_publisher    = self.create_publisher(PointCloud, 'pf/pose_ned/pointcloud', 10)
   
    def __publish_particlePointCloud(self):
        '''
            Function to publish particle point cloud for visualization in rviz
        '''

        # Get vector of particle poses
        pf_poseVec = self.__localizationFilter.getPFParticlePoseVector()

        # Define message
        pf_particlePCMsg = PointCloud()
        channel = ChannelFloat32()
        point = Point32()

        # Fill header
        pf_particlePCMsg.header.stamp = self.get_clock().now().to_msg()
        pf_particlePCMsg.header.frame_id = 'map_idl'

        # Fill channel name of message
        channel.name = 'intensity'
        intensity = 128

        # Initialize empty lists
        ptList = []
        chList = []

        # Fill point data
        for ii in range(self.__pfPointcloudSize):
            # Fill channel data
            chList.append(float(intensity))

            # Append new point to list of points
            ptList.append(Point32())

            # Fill Point data
            ParticlePose = pf_poseVec[:, ii].copy()
            ptList[ii].x = float(ParticlePose[0])
            ptList[ii].y = float(ParticlePose[1])
            ptList[ii].z = float(ParticlePose[2])

        # Set channel values to chList
        channel.values = chList

        # Populate pf message channels
        pf_particlePCMsg.channels = [channel]

        # Set points to list of Point32
        pf_particlePCMsg.points = ptList

        # Publish message
        self.__pose_pointcloud_publisher.publish(pf_particlePCMsg)

    def __publish_pose(self):
        '''
            Gets filter pose and variance from localizationFilter then publishes them as a PoseWithCovarianceStamped message
        '''

        # Only publish if systemReady bool is set
        if self.__systemReadyIndicator == True:
            # Get pose and variance
            pose_n_nb = self.__localizationFilter.getFilterPoseNED()
            var = self.__localizationFilter.getFilterVarianceNED()

            # Extract positions and angle from pose 
            pos_n_nb = pose_n_nb[:3,0].reshape(3,1)
            psi_n_nb = pose_n_nb[3,0]

            # Setup diagonal matrix with variances of X-, Y-, Z-, and rotZ
            var_n_nb = np.zeros((6,6), dtype = np.float32)
            var_n_nb[0,0] = var[0,0]
            var_n_nb[1,1] = var[1,0]
            var_n_nb[2,2] = var[2,0]
            var_n_nb[5,5] = var[3,0]

            # Create empty message
            bodyPoseMessage = PoseWithCovarianceStamped()

            # Assign timestamp
            timeNow = self.get_clock().now().to_msg()
            bodyPoseMessage.header.stamp = timeNow

            #### Populate Pose ####
            ### Positions ###
            posToMsg_n_nb = pos_n_nb.astype(np.float64)
            bodyPoseMessage.pose.pose.position.x = posToMsg_n_nb[0, 0]
            bodyPoseMessage.pose.pose.position.y = posToMsg_n_nb[1, 0]
            bodyPoseMessage.pose.pose.position.z = posToMsg_n_nb[2, 0]
            bodyPoseMessage.pose.covariance = var_n_nb.reshape(36).astype(np.float64)

            ### Quaternions ###
            ## Not actually a quaternion, but roll/pitch/yaw ##
            psiToMsg_n_nb = psi_n_nb.astype(np.float64)
            bodyPoseMessage.pose.pose.orientation.x = 0.0
            bodyPoseMessage.pose.pose.orientation.y = 0.0
            bodyPoseMessage.pose.pose.orientation.z = psiToMsg_n_nb
            bodyPoseMessage.pose.pose.orientation.w = -2.0

            self.__pose_publisher.publish(bodyPoseMessage)

    # Callbacks

    def __localization_callback(self):
        '''
            Runs localize method of localizationFilter, creates pose histograms from particle poses and weights
            and smooths them with a gaussian kernel (histSmoothingKernel -> param of localizationFilter),
            saves most likely pose and the particle variance around this pose to filter variables
        '''
        
        if self.__systemReadyIndicator == True:
            # Run  localize
            self.__localizationFilter.localize()

            # Publish pose
            self.__publish_pose()

    def __propogation_callback(self):
        '''
            Function to call pf.propagate with set rate
        '''

        # Only propagate if kf has sent ready signal
        if self.__systemReadyIndicator:
            # Propogate particles
            self.__localizationFilter.propagate(self.prop_dt)
  
    def __pointcloud_callback(self, pc_msg):
        '''
            Callback function to run each timne a new pointcloud is received from the camera
        '''

        if self.__systemReadyIndicator == True:
            # Parse pointcloud 
            pointcloud_from_msg = pc2.pointcloud2_to_xyz_array(pc_msg)

            # Call update with new pointcloud
            self.__localizationFilter.pointcloud_update(pointcloud_from_msg)     

    def __velocity_callback(self, vel_msg):
        '''
            Callback function to run when a new velocity from the KF is received
        '''

        # Get velocities from message
        lin_vel_from_msg = vel_msg.twist.twist.linear
        ang_vel_from_msg = vel_msg.twist.twist.angular
        cov_from_msg = vel_msg.twist.covariance

        # Set up velocity-vector from message
        velVec_l = np.array([[lin_vel_from_msg.x],
                             [lin_vel_from_msg.y],
                             [lin_vel_from_msg.z],
                             [ang_vel_from_msg.z]], dtype=np.float32)

        # Calculate vector of std.deviations from covariance-matrix
        velCov_l = np.array([[cov_from_msg[0]],
                             [cov_from_msg[7]],
                             [cov_from_msg[14]],
                             [cov_from_msg[35]]], dtype=np.float32)

        # Gets time from msg
        timeNsec = Time.from_msg(vel_msg.header.stamp).nanoseconds
        timeSec = timeNsec*10**(-9)
 

        # Set velocities and std dev to filter
        self.__localizationFilter.setPropagationVelocityWithCovariance( velVec_l,
                                                                        velCov_l,
                                                                        timeSec)     

    def __roll_pitch_callback(self, rp_msg):
        '''
            Callback function to update roll and pitch values from KF
        '''
        # Get and set orientation from GT message.
        orient_from_msg = rp_msg.pose.pose.orientation
        roll = orient_from_msg.x
        pitch = orient_from_msg.y
        statusMsg = orient_from_msg.w

        # Construct vector
        tHeta_bl = np.array([[roll],[pitch],[0.0]], dtype=np.float32)

        # If w from quat is -3, set bool ekfOnline false
        if statusMsg < -2.5:
            ekfOnline = False

        else:
            ekfOnline = True

        # Pass to filter
        self.__localizationFilter.setEKFLinearOnlineState(ekfOnline)
        self.__localizationFilter.setCurrentRollPitchAngles(tHeta_bl)

    def __systemReset_callback(self, reset_msg):
        '''
            Callback to reset LocalizationFilter to initial state
        '''
        # If msg.data is true, reset filter
        if reset_msg.data == True:
            # Call function to reset filter to initial pose
            self.__localizationFilter.resetParticleFilterToInitPose()
            self.get_logger().info("Particle filter reset")

    def __systemStart_callback(self, systemStart_msg):
        '''
            Callback to set kf indicator
        '''

        # Sets the indicator at first true message
        if systemStart_msg.data == True:
            self.__systemReadyIndicator = True


def main(args=None):
    rclpy.init(args=args)

    pointCloudSubscriber = PF_ros_node()

    rclpy.spin(pointCloudSubscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    pointCloudSubscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

