# Other imports
import numpy as np
import time

# Own stuff
# Pf filter stuff
from idl_pf_pkg.JitParticleFilterClass import *
from idl_pf_pkg.LocalizationFilter import *
# Geometric data
from idl_transform_pkg.droneConfiguration import DroneGeometry, Rot

# Filter Specifications
from idl_botsy_pkg.filterConfig import FilterInitialStates, FilterConfiguration, ParticleFilterSetup



def main():
    # Create filter configuration
    filterParams = LocalizationFilterParams()

    # Set relevant data for benchmarking
    filterParams.number_of_particles = 1000

    # Filter configuration
    # Pointcloud update
    filterParams.nPts_PC = 50
    filterParams.pcUpdateSqSum = False
    
    # Pointcloud downsample
    filterParams.pcdsRandPoints = True
    filterParams.pcdsLoopSelect = False
    filterParams.pcdsLoopSelMaxLoops = 2.0*filterParams.nPts_PC

    ## Create Localization filter ##
    locFilter = LocalizationFilter(filterParams)
    

    # Create dummy data for filter
    filter_dummyVelocity    = np.random.random((4,1)).astype(np.float32)
    filter_dummyDt          = 0.1

    filter_dummyPointCloudScaleFactor   = np.float32(10.0)
    filter_dummyPointCloud              = np.random.random((9000,3)).astype(np.float32)*filter_dummyPointCloudScaleFactor


    # Set number of loops
    numOuterLoops = 100
    numLoopsPerFunc = 1

    # Initialize empty lists
    propagationTime = []
    updateTime = []



    for ii in range(numOuterLoops + 1):

        print("Outer loop iteration: " + str(ii))
        
        for jj in range(numLoopsPerFunc):
            # Set velocity to filter
            filter_dummyVelocity    = np.random.random((4,1)).astype(np.float32)
            filter_dummyCov         = np.random.random((4,1)).astype(np.float32)

            locFilter.setPropagationVelocityWithCovariance(filter_dummyVelocity, filter_dummyCov, 1.0)

            # Record time before propagate
            beforeTime = time.time()

            # Propagate
            locFilter.propagate(filter_dummyDt)

            # deltaT
            deltaT = time.time() - beforeTime

            # Save time to vec
            propagationTime.append(deltaT)



        for kk in range(numLoopsPerFunc):
            # Record time before update
            beforeTime = time.time()

            # Update, noralize and resample
            locFilter.pointcloud_update(filter_dummyPointCloud)

            # deltaT
            deltaT = time.time() - beforeTime

            # Save time to vec
            updateTime.append(deltaT)

    # Save vectors as files
    print("Propagation time:")
    print(propagationTime[1:])

    print("Update time:")
    print(updateTime[1:])

    np.savetxt( "propTime.csv", 
                propagationTime[1:],
                delimiter =", ", 
                fmt ='% s')

    np.savetxt( "updateTime.csv", 
                updateTime[1:],
                delimiter =", ", 
                fmt ='% s')

if __name__ == '__main__':
    main()

